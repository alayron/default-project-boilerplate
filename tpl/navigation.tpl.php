<header class="header clearfix">
	<nav class="main-navigation">
		<figure class="logo  main-navigation__logo">
			<a href="<?=HTTP_ROOT?>">
				<img src="<?=HTTP_ROOT?>/img/logo/ipsystems/white.png">
			</a>
		</figure>
		<ul class="menu  main-navigation__menu  taright">
		<?php
			$main_navigation = GlobalStructure::Instance()->get_main_level_navigation_objects();
			
			foreach ( $main_navigation as $menu_item ) {
				echo '<li class="menu__link  main-navigation__link"><a href="'.HTTP_ROOT.'/'.$menu_item->id.'">'.$menu_item->label.'</a></li>';
			}
		?>
		</ul>
	</nav>
	<div class="teaser wrapper--base">
		<figure class="call2action tacenter">
			<h1 class="call2action__headline">Get this benefit, today:</h1>
			<p class="call2action__desc">Complete this action, that calls out what you came here for:</p>
			<a class="call2action__action bgcolor--primary" href="#">do action</a>
		</figure>
	</div>
</header>