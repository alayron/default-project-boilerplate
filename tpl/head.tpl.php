<?php
	$GS = GlobalStructure::Instance();
	$GS->head_var('page_title','Default Template','!combine');
	
?><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?=join(' ', array_reverse($GS->head_var('page_title')))?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link href="<?=HTTP_ROOT?>/favicon.ico" rel="icon" type="image/x-icon" />
    <link rel="apple-touch-icon" href="apple-touch-icon-precomposed.png">
    <link rel="apple-touch-icon" sizes="57x57" href="apple-touch-icon_57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon_72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="apple-touch-icon_76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon_114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="apple-touch-icon_120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon_144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="apple-touch-icon_152x152.png">
    <link rel="stylesheet" href="<?=HTTP_ROOT?>/css/screen.css">
    <link rel="stylesheet" href="<?=HTTP_ROOT?>/css/shame.css">
    <!--[if lte IE 9]><link rel="stylesheet" href="css/ie.css"><![endif]-->
    <script src="<?=HTTP_ROOT?>/bower_components/modernizr/modernizr.js"></script>
	<script>window.jQuery || document.write('<script src="<?=HTTP_ROOT?>/bower_components/jquery/jquery.min.js"><\/script>')</script>
</head>