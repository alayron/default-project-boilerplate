<?php
	// turns of Notices in php error reporting
	error_reporting(E_ALL ^ E_NOTICE);
		
	require('inc/config.inc.php');	
	require('inc/global.inc.php');
		
	//Streamed files belonging to articles
	if($_REQUEST['content'] && $_REQUEST['id'] && strpos($_REQUEST['label'],'.') !== false) {
		define('IS_FILE_REQUEST', true);
		//Include _id.tpl.php from article to decide what to do
		if($path = GlobalStructure::Instance()->get_path_object($_REQUEST['content'])) {
			$path_to_id_tpl = FILE_PATH.'/'.GlobalStructure::path_object_to_path_string($path).'/_id.tpl.php';
			if(file_exists($path_to_id_tpl)) {
				include($path_to_id_tpl);
			}
		}
		//requested article type does not exist
		header("HTTP/1.0 404 Not Found");
		exit();	
	}
	define('IS_FILE_REQUEST', false);
		
?><!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<?php
		//default header / footer information
		$GS = GlobalStructure::Instance();
		$GS->set_content('');
		$GS->push_context('head','tpl/head.tpl.php');
		$GS->push_context('navigation','tpl/navigation.tpl.php');
		$GS->push_context('footer','tpl/footer.tpl.php');

		
		//determine the content template
		if ( $_REQUEST['content'] ) {
			// Is the requested content NOT part of the navigation object?
			if ( ! $navigation_path = $GS->get_path_object($_REQUEST['content']) ) {
				$GS->set_content('content/404.tpl.php');
			} else {
				$content = FILE_PATH.'/'.GlobalStructure::path_object_to_path_string($navigation_path);
				// Is the requested content a directory?
				if ( is_dir($content) ) {
					// Is the requested content part of an article collection
					if(file_exists($content.'/_id.tpl.php')) {
						$GS->set_content($content.'/_id.tpl.php');
					} else {
						// Is there a landing.tpl.php in the requested directory?
						if ( file_exists($content.'/landing.tpl.php') ){
							$GS->set_content($content.'/landing.tpl.php');
						} else {
							$GS->set_content('content/404.tpl.php');
						}
					}
				// not a directory.. is it maybe a *.tpl.php?
				} elseif ( file_exists($content.'.tpl.php') ) {
					$GS->set_content($content.'.tpl.php');
				} else {
					$GS->set_content('content/404.tpl.php');
				}
				
				//Set custom header footer navigation etc
				$GS->upadte_custom_context();
				
			}
		// Nevermind, take me to the homepage
		} else {
			$GS->set_content('content/home.tpl.php');
		}
		
	
		// Lets put it together
	
		// Head
		$GS->include_head();
	?>
    <body class="fill--color-bg">
		<?php
			
			echo '<div class="wrapper_global">';
			
			// Includes the navigation
			$GS->include_navigation();

			// Includes the selected content
			$GS->include_content();
			
			echo '<div class="wrapper_global--footer"></div>
			</div>';
			
			// Includes the footer
			$GS->include_footer();
		?>
    </body>
    <?php require('tpl/foot.tpl.php'); ?>
</html>
