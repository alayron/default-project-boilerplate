<?php

	final class GlobalStructure {
		
		//content context
		private $context = Array();
		
		private $structure = null;
		private $json = 
			'{
				"label":"Default Project Boilerplate",
				"id":"content",
				"submenu": [
					{
						"label":"Page",
						"id":"page",
						"submenu": [
							{
								"label":"Subpage One",
								"id":"subpage_one"
							}, {
								"label":"Subpage Two",
								"id":"subpage_two"
							}
						]
					}, {
						"label":"Impressum",
						"id":"impressum"
					}, {
						"label":"Artikel",
						"id":"article",
						"HEAD_VARS":{
							"page_title": { "value":"more important", "mode":"!replace" }
						}
					}
				]
			}';
		
		
		
		/**
		 *  @retval object
		 *   singleton 
		 */
		public static function Instance() {
		    static $inst = null;
		    if ($inst === null) {
		        $inst = new GlobalStructure();
		    }
		    return $inst;
		}
		
		private function __construct() {
			$this->structure = json_decode($this->json);
		}
		
		/**
		 *  @retval object
		 *   the global site structure as object
		 */
		public function get_global_structure() {
			return $this->structure;
		}
		
		/**
		 *  @retval string
		 *   the global site structure as json string
		 */
		public function get_global_json() {
			return $this->json;
		}
		
		/**
		 *  @retval mixed (array|false)
		 *   returns the array of the toplevel node objects of the sitestructure
		 */
		public function get_main_level_navigation_objects() {
			return $this->structure->submenu;
		}
		
		/**
		 *  @retval mixed (array|false)
		 *   returns the array of parent nodes including the $_content_id node
		 */
		public function get_path_object($_content_id) {
			$tmp = $this->get_path_object_rec(Array($this->structure), $_content_id);
			if(!is_Array($tmp)) {
				return false;
			} else {
				return array_reverse($tmp);
			}
		}
		
		public function push_context($_key,$_path_to_file) {
			if(!is_array($this->context[$_key])) $this->context[$_key] = Array();
			$this->context[$_key][] = $_path_to_file;
		}
		
		public function set_content($_content) {
			$this->context['content'] = $_content;
		}
		
		public function include_content() {
			include($this->context['content']);
		}
		
		public function include_head() {
			if(count($this->context['head']))
				include(array_pop($this->context['head']));
		}
		
		public function include_navigation() {
			if(count($this->context['navigation']))
				include(array_pop($this->context['navigation']));
		}
		
		public function include_footer() {
			if(count($this->context['footer']))
				include(array_pop($this->context['footer']));
		}
		
		public function upadte_custom_context($_items = null) {
		
			//Initialize context value container
			if(!$this->context['HEAD_VARS'])
					$this->context['HEAD_VARS'] = Array();
			if(!$this->context['NAVIGATION_VARS'])
					$this->context['NAVIGATION_VARS'] = Array();
			if(!$this->context['FOOTER_VARS'])
					$this->context['FOOTER_VARS'] = Array();
			
			if(!is_array($_items)) {
				$_items = $this->get_path_object($_REQUEST['content']);
			}
			if(!$_items || !count($_items))
				return;
			for($i = 0; $i < count($_items); $i++) {
				$path = FILE_PATH.'/'.GlobalStructure::path_object_to_path_string($_items);
				if(is_dir($path)) {
					if(file_exists($path.'/_head.tpl.php'))
						$this->push_context('head',$path.'/_head.tpl.php');
				}
				
				$item = array_pop($_items);
				
				if($item->HEAD_VARS) {
					$vars = (array)$item->HEAD_VARS;
					foreach($vars as $k => $v) {
						$this->write_value('HEAD_VARS', $k, $v->value, $v->mode, 1);		
					}
				}
			}
		}		
		
		
		private function write_value($_scope, $_key, $_value, $_mode, $_less_important = 0) {
			if(!$this->context[$_scope][$_key]) $this->context[$_scope][$_key] = Array();
			if(preg_match('/(replace|combine|unique)$/', $_mode, $_matches)) {
				$mode = $_matches[1];
				$i = strlen($_mode) - (strlen($mode) + $_less_important);
				if(intval($this->context[$_scope][$_key]['i']) < $i || !isset($this->context[$_scope][$_key]['i'])) {
					$this->context[$_scope][$_key]['i'] = $i;
					$this->context[$_scope][$_key]['mode'] = $mode;
					switch($mode) {
						case 'replace':
							$this->context[$_scope][$_key]['value'] = is_array($_value) ? $_value : Array($_value); 
						break;
						case 'combine':
						case 'unique':
							if(!is_array($this->context[$_scope][$_key]['value'])) {
								$this->context[$_scope][$_key]['value'] = Array($this->context[$_scope][$_key]['value']);	
							} 
							$this->context[$_scope][$_key]['value'] = array_merge($this->context[$_scope][$_key]['value'],is_array($_value) ? $_value : Array($_value));
							if($mode == 'unique') {
								$this->context[$_scope][$_key]['value'] = array_unique($this->context[$_scope][$_key]['value']);
							}
						break;
					}
				}
			}
		}
		
		public function head_var($_key, $_value = null, $_mode = 'replace') {
			if($_value === null) {
				//return value
				if($this->context['HEAD_VARS'][$_key])
					return $this->context['HEAD_VARS'][$_key]['value'];
				return false;
			} else {
				//write value
				$this->write_value('HEAD_VARS', $_key, $_value, $_mode);
			} 
		}
		
		public function navigation_var($_key, $_value) {
			if($_value === null) {
				//return value
				if($this->context['NAVIGATION_VARS'][$_key])
					return $this->context['NAVIGATION_VARS'][$_key]['value'];
				return false;
			} else {
				//write value
				$this->write_value('NAVIGATION_VARS', $_key, $_value, $_mode);
			} 
		}
		
		public function footer_var($_key, $_value) {
			if($_value === null) {
				//return value
				if($this->context['FOOTER_VARS'][$_key])
					return $this->context['FOOTER_VARS'][$_key]['value'];
				return false;
			} else {
				//write value
				$this->write_value('FOOTER_VARS', $_key, $_value, $_mode);
			} 
		}
		
		
		
		public function super_head() {			
			if(count($this->context['head']))
				include(array_pop($this->context['head']));
		}
		
		public function super_navigation() {
			if(count($this->context['navigation']))
				include(array_pop($this->context['navigation']));
		}
		
		public function super_footer() {
			if(count($this->context['footer']))
				include(array_pop($this->context['footer']));
		}
		
		
		
		
		/**
		 *  @retval boolean
		 *   returns publish state according to publish date / hidden etc
		 */
		public function is_article_published($_content_id, $article_id) {

		}
		
		/**
		 *  @retval mixed (string|false)
		 *   returns the path of the article directory
		 */
		public function get_article_path($_content_id, $_article_id, $_check_for_publihed = false) {
			//check shared memory key
			/*$shm_key = ftok(__FILE__, 't');
			$shm_id = shmop_open($shm_key, "c", 0644, 10000);
			if($shm_id) {
				if( shmop_read($shm_id, 0, 8) == "Fyu67amz" ) {
					$data = shmop_read($shm_id, 7, 10000-8) {
						$ac_info = json_decode($data);
						foreach($ac_info as $ac_item) {
							if($ac_item->content_id == $ac_item && $ac_item->article_id == $article_id) {
								if( $ac_item->time < time() + 50 ) {
									return $ac_path;
								}
							} else {
								
							}
						}
					}
				}
			}*/
			
			//get path of articles container
			$path = $this->get_path_object($_content_id);
			if(!$path || !is_dir($path_str = FILE_PATH.'/'.GlobalStructure::path_object_to_path_string($path))) return false;
			
			//get path of article container
			$articles_dir = opendir($path_str);
			$article_dir_name = false;
			if($articles_dir) {
				while($file = readdir($articles_dir)) {
					if($file != '.' && $file != '..' && is_dir($path_str.'/'.$file) && strpos($file,'_')) {
						$matching = preg_match('/^([0-9]+)_/', $file, $matches);
						if($matching && intval($matches[1]) == $_article_id) {
							$article_dir_name = $file;
							break;
						}
					}
				}
				closedir($articles_dir);
			}
			if($article_dir_name === false || $path === false) return false;
			
			if($_check_for_publihed) {
				//todo check for published information
				return false;
			}
		
			return GlobalStructure::path_object_to_path_string($path).'/'.$article_dir_name;
		}
		
		
		
		/**
		 *	@param mixed $_item 
		 *	 array path object | string content_id
		 *  @retval mixed (array|false)
		 *   returns the nodes that are on the same level as the selected one
		 */
		public static function get_siblings($_item) {
			if(!is_array($_item)) {
				$_item = GlobalStructure::Instance()->get_path_object($_item);
			}
			array_pop($_item);
			return array_pop($_item)->submenu;
		}
		
		/**
		 *	@param mixed $_item 
		 *	 array path object | string content_id
		 *  @retval string
		 *   returns path as string
		 */
		public static function path_object_to_path_string($_item) {
			if(!is_array($_item)) {
				$_item = GlobalStructure::Instance()->get_path_object($_item);
			}
			$out = Array();
			foreach($_item as $o) {
				$out[] = $o->id;
			}
			return implode('/', $out);
		}
		
		private function get_path_object_rec($_elm, $_content_id) {
			foreach($_elm as $subelement) {
				if($subelement->id == $_content_id) return Array($subelement);
				if($subelement->submenu) {
					$tmp = $this->get_path_object_rec($subelement->submenu, $_content_id);
					if($tmp){
						array_push($tmp, $subelement);
						return $tmp;
					} 
				}
			}	
		}
		
		public static function filsystem_base_article_get_list($_content_id, $_include_unpublished = false) {
			
			if($path = GlobalStructure::path_object_to_path_string($_content_id)) {
				$article_container_dir = opendir(FILE_PATH.'/'.$path);
				if($article_container_dir) {
					$articles = Array();
					while($f = readdir($article_container_dir)) {
						if($f != '.' && $f != '..' && preg_match('/^([0-9]+)/', $f, $matches) && is_dir(FILE_PATH.'/'.$path.'/'.$f)) {
							$article = null;
							$content_file_path = FILE_PATH.'/'.$path.'/'.$f.'/content.tpl.php';
							if(file_exists($content_file_path)) {
								$PREVENT_OUTPUT = true;
								include($content_file_path);
								if($article) {
									//todo publish check
									$article = (array)$article;
									$article["dir_name"] = $f;
									$article["full_path"] = $path.'/'.$f;
									$article["has_thumbnail"] = file_exists(FILE_PATH.'/'.$path.'/'.$f.'/thumb.jpg') ? true : false;
									$articles[] = $article;
								}
							}
						}
					}
					closedir($article_container_dir);
					return $articles;
				}
			
			}
			return  Array();
		}
		
		//handles filerequest for filesysten based article
		public static function filesystem_based_article_handle_filerequest() {
			global $_ALLOWED_EXTENTIONS_FOR_ARTICLE_FILES;
			if(in_array(strtolower(pathinfo($_REQUEST['label'], PATHINFO_EXTENSION)), $_ALLOWED_EXTENTIONS_FOR_ARTICLE_FILES)) {
				//Check for valid filename 
				if( preg_match('#^([a-zA-Z0-9_\-/\.]*)$#',$_REQUEST['label']) ) {
					if(strpos($_REQUEST['label'],'..') === false){
						//Get Dir name
						$path_to_article_dir = GlobalStructure::Instance()->get_article_path($_REQUEST['content'],$_REQUEST['id']);
						if($path_to_article_dir !== false) {
							//check if the file actually exists
							$filepath = FILE_PATH.'/'.$path_to_article_dir.'/'.$_REQUEST['label'];
							if(file_exists($filepath)) {
								header('Content-Type: '.mime_content_type($filepath));
								readfile($filepath);
								exit();
							} else {
								//the requestet file does not exist!
								header("HTTP/1.0 404 Not Found");
								echo '404';
								exit();	
							}
						} else {
							//the requestet article does not exist or is not published
							header("HTTP/1.0 404 Not Found");
							echo '404/403';
							exit();	
						}
					}
				}
				//NoNo u r trying fuu with the filename no .. or strange chars allowed! 
				header("HTTP/1.0 404 Not Found");
				//echo 'NoNo';
				exit();	
				
			} else {
				//This file extention is not allowed!	
				header("HTTP/1.0 404 Not Found");
				exit();	
			}
		}
	}

?>