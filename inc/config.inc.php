<?php

/* ====================================================
	path constants
	================================================= */
	
	$project_name = 'default_project_boilerplate';
	
	// Configurations for Remote
	if( $_SERVER['HTTP_HOST'] == 'sw.dev.e05.ip-systems.at' ) {
		define('HTTP_ROOT', '/'.$project_name);
		define('FILE_PATH', '/var/www/'.$project_name);
	// Configurations for Local
	} else {
		define('HTTP_ROOT', '/'.$project_name);
		define('FILE_PATH', '/Library/WebServer/Documents/'.$project_name);
	}

/* ====================================================
	article files
	================================================= */
	
	$_ALLOWED_EXTENTIONS_FOR_ARTICLE_FILES = Array('jpg','png','gif','jpeg','doc','pdf','mov','mp4','mp3','swf','ogg','m4v','m4a');