<section class="home__intro  content--base">
	<div class="wrapper--base">
		<figure class="tacenter">
			<p class="intro  ctx--home">This Product does a certain thing. It helps you reaching this goal, while overcoming what the concurrence couldn't.</p>
			<a class="call2action__action  bgcolor--primary  tacenter" href="<?=HTTP_ROOT?>/page">Tell me more</a>
		</figure>
	</div>
</section>
<section class="content--base  home__benefits">
	<div class="wrapper--base">
		<figure class="bullet bullet--thirds  tacenter">
			<figcaption class="bullet__desc">
				<h1>Conversations</h1>
				<p>This product comes with a lot of chit-chat. There is an active community, providing a lot of written content consumable for you.</p>
			</figcaption>
		</figure>
		<figure class="bullet bullet--thirds  tacenter">
			<figcaption class="bullet__desc">
				<h1>Paperplanes</h1>
				<p>This product comes with a lot of chit-chat. There is an active community, providing a lot of written content consumable for you.</p>
			</figcaption>
		</figure>
		<figure class="bullet bullet--thirds  tacenter">
			<figcaption class="bullet__desc">
				<h1>An Umbrella</h1>
				<p>This product comes with a lot of chit-chat. There is an active community, providing a lot of written content consumable for you.</p>
			</figcaption>
		</figure>
	</div>
</section>
<section class="content--base  focus vers--camera  home__focus bgcolor--primary">
	<div class="wrapper--base">
		<div class="focus__content  txtcolor--white">
			<h1>Focused Feature</h1>
			<p>We widely display this particular feature, to catch your attraction and to leave an emotional impression. Big Images work very well in this context.</p>
			<a class="call2action__action vers--outlined  tacenter" href="<?=HTTP_ROOT?>/page">More about that</a>
		</div>
	</div>
</section>
<section class="content--base  home__highlighted-feature">
	<div class="wrapper--base">
		<figure class="highlighted-feature">
			<div class="highlighted-feature__imagery  tacenter">
				<img src="<?=HTTP_ROOT?>/img/home/onion.jpg" class="circle maauto">
			</div>
			<figcaption class="highlighted-feature__desc  tacenter">
				<h1 class="txtcolor--secondary">A freakin Onion</h1>
				<p>This Block aims to highlight one paricular feature or core concept.<br>
				The User came here, to solve a Problem with THIS special feature:</p>
			</figcaption>
		</figure>
		<div class="clearfix clear">
			<figure class="bullet bullet--halfs  tacenter">
				<figcaption class="bullet__desc">
					<h2>Conversations</h2>
					<p>This product comes with a lot of chit-chat. There is an active community, providing a lot of written content consumable for you.</p>
				</figcaption>
			</figure>
			<figure class="bullet bullet--halfs  tacenter">
				<figcaption class="bullet__desc">
					<h2>Conversations</h2>
					<p>This product comes with a lot of chit-chat. There is an active community, providing a lot of written content consumable for you.</p>
				</figcaption>
			</figure>
			<figure class="bullet bullet--halfs  tacenter">
				<figcaption class="bullet__desc">
					<h2>Conversations</h2>
					<p>This product comes with a lot of chit-chat. There is an active community, providing a lot of written content consumable for you.</p>
				</figcaption>
			</figure>
			<figure class="bullet bullet--halfs  tacenter">
				<figcaption class="bullet__desc">
					<h2>Conversations</h2>
					<p>This product comes with a lot of chit-chat. There is an active community, providing a lot of written content consumable for you.</p>
				</figcaption>
			</figure>
		</div>
		<div class="call2action  tacenter">
			<a class="call2action__action  bgcolor--secondary">
				Shut up and take my money
			</a>
		</div>
	</div>
</section>
<section class="content--base  home__focus vers--skateboard  focus">
	<div class="wrapper--base">
		<div class="focus__content focus__content--lessspace  last txtcolor--white">
			<h1>Focused Feature</h1>
			<p>We widely display this particular feature, to catch your attraction and to leave an emotional impression. Big Images work very well in this context.</p>
			<p>&nbsp;</p>
			<figure class="bullet">
				<div class="bullet__imagery vers--icon">
					<i class="fa fa-send-o"></i>
				</div>
				<figcaption class="bullet__desc">
					<h2>Conversations</h2>
					<p>This product comes with a lot of chit-chat. There is an active community, providing a lot of written content consumable for you.</p>
				</figcaption>
			</figure>
			<figure class="bullet">
				<div class="bullet__imagery vers--icon">
					<i class="fa fa-puzzle-piece"></i>
				</div>
				<figcaption class="bullet__desc">
					<h2>Conversations</h2>
					<p>This product comes with a lot of chit-chat. There is an active community, providing a lot of written content consumable for you.</p>
				</figcaption>
			</figure>
			<a class="call2action__action vers--outlined  tacenter" href="<?=HTTP_ROOT?>/page">More about that</a>
		</div>
	</div>
</section>