<?php

	//handle filerequests for article resources
	if(IS_FILE_REQUEST) {
		GlobalStructure::filesystem_based_article_handle_filerequest();
	}

	
	if(intval($_REQUEST['id'])) {
		//article request
		$path_to_article_dir = GlobalStructure::Instance()->get_article_path($_REQUEST['content'],$_REQUEST['id']);
		if($path_to_article_dir !== false && file_exists($path_to_article_dir.'/content.tpl.php')) { 
			include($path_to_article_dir.'/content.tpl.php');
		} else {
			echo '404';
		}
	} else {
		//index request
		include('landing.tpl.php');
	}
	
	